package com.mhk.cmt.model;

import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.transaction.Transactional;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;

@Data
@Entity
@Transactional
@Table(name = "userprofile")
public class UserProfile implements UserDetails {
	/**
	 * UUID
	 */
	private static final long serialVersionUID = -6172424245885224276L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String paswd;
	private String activeRole;
	private String email;
	private String clientId;
	private String redirectUrls;

	/*
	 * private List<Role> roles; private List<Module> module; private
	 * List<Permission> permmissions;
	 */

	@Override
	public String toString() {
		return "UserProfile [name=" + name + ", email=" + email + "]";
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * public List<Role> getRoles() { return roles; }
	 * 
	 * public void setRoles(List<Role> roles) { this.roles = roles; }
	 * 
	 * public List<Module> getModule() { return module; }
	 * 
	 * public void setModule(List<Module> module) { this.module = module; }
	 * 
	 * public List<Permission> getPermmissions() { return permmissions; }
	 * 
	 * public void setPermmissions(List<Permission> permmissions) {
	 * this.permmissions = permmissions; }
	 */
}
