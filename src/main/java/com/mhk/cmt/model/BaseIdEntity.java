/**
 * 
 */
package com.mhk.cmt.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * @author sanskriti.agarwal
 *
 */

@MappedSuperclass
public class BaseIdEntity {

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	protected String id;
}
