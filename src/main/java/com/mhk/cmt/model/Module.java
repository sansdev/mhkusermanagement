package com.mhk.cmt.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Aaina arora
 */

@Entity
@Table(name="module")
public class Module implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long Id;
	
	@NotNull
	@Column(unique = true)
	private String moduleName;
	
	@NotNull
	private String description;
	
	
	@Column(name = "status")
	private StatusEnum status = StatusEnum.ACTIVE;

	@Column(name= "is_deleted")
	private boolean isDeleted = false;   
	
	private String createdBy;
	private Date createdAt = new Date();
	private String updatedBy;
	
	
	private Date updatedAt = new Date();
	
	@PreUpdate
	public void setupdatedAt() {  this.updatedAt = new Date(); }

	
	public Module() {
		
	}
	
	public Module(Long id, String moduleName, String description, StatusEnum status, boolean isDeleted,
			String createdBy, Timestamp createdAt, String updatedBy, Timestamp updatedAt) {
		super();
		Id = id;
		this.moduleName = moduleName;
		this.description = description;
		this.status = status;
		this.isDeleted = isDeleted;
		this.createdBy = createdBy;
		this.createdAt = createdAt;
		this.updatedBy = updatedBy;
		this.updatedAt = updatedAt;
	}

	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public StatusEnum getStatus() {
	    return status;
	}

	public void setStatus(StatusEnum status) {
	    this.status = status;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	enum StatusEnum {
		ACTIVE, INACTIVE
	}	

}
