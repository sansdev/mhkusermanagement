package com.mhk.cmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
@Configuration
@ComponentScan({"com.mhk.cmt.*","com.mhk.cmt.*","com.mhk.cmt.controllers","com.mhk.cmt.services"})
public class App extends WebSecurityConfigurerAdapter{

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
	
	
	  @Override protected void configure(HttpSecurity http) throws Exception {
	  http.csrf().disable() .antMatcher("/**").authorizeRequests()
	  .antMatchers("/", "/user/*","/chat/*","/index.html").permitAll()
	  .anyRequest().authenticated();
	  
	  }
	 

	

	@RequestMapping("/")
	public String defaultPage() {
		
		//return "Welcome "+" to Spring Security Demo App"+ "<br/><a href=\"securedPage\">Login with Local Auth Server</a>";
		return "index";
	}

	

}
