package com.mhk.cmt.config;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfig {

	@Bean
	public DataSource getDataSource() {

		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
		dataSourceBuilder.driverClassName("com.mysql.jdbc.Driver");
		dataSourceBuilder.url("jdbc:mysql://localhost:3306/mhkcmt");
		dataSourceBuilder.username("sans");
		dataSourceBuilder.password("sans");

		/*
		 * EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder(); return
		 * builder .setType(EmbeddedDatabaseType.HSQL) .build();
		 */
		return dataSourceBuilder.build();
	}

	/*
	 * public DataSource getCloudDataSource() {
	 * 
	 * DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create(); //
	 * dataSourceBuilder.driverClassName("com.mysql.jdbc.Driver");
	 * dataSourceBuilder.url(
	 * "http://database-1.ceyrfmpvlzbk.us-east-1.rds.amazonaws.com:3306/");
	 * dataSourceBuilder.username("admin");
	 * dataSourceBuilder.password("mhk.cmt1234");
	 * 
	 * 
	 * 
	 * EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder(); return
	 * builder .setType(EmbeddedDatabaseType.HSQL) .build();
	 * 
	 * return dataSourceBuilder.build(); }
	 */
}