/**
 * 
 */
package com.mhk.cmt.services;

import java.net.MalformedURLException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mhk.cmt.dto.LoginRequestDTO;
import com.mhk.cmt.util.AWSCognitoUtil;
import com.microsoft.aad.adal4j.AuthenticationContext;
import com.microsoft.aad.adal4j.AuthenticationResult;

/**
 * @author sanskriti.agarwal
 *
 */
@Service
public class AzureADUserLoginService {

	private static final String AUTHORITY = "";
	private String AZURE_CLIENT_ID = "e2569155-44b8-42ba-98b3-44cda71c82ab";
	public String AZURE_TENANT_ID = "90bafee9-32b4-4d0d-a916-3effc9596750";
	public String AZURE_OBJECT_ID = "db87dbb9-f10b-4b26-93df-e75d745a7f81";
	public String AZURE_CLIENT_SECRET = "d7Vft.-DcFt.qd8KUw_7-2e~5RGL0VPVkC";

	@Autowired
	public AWSCognitoUtil util;

	public String loginUser(LoginRequestDTO loginRequest)
			throws MalformedURLException, InterruptedException, ExecutionException {
		ExecutorService service = Executors.newFixedThreadPool(1);

		AuthenticationContext context = new AuthenticationContext(AUTHORITY, false, service);

		String username = loginRequest.getUsername();
		String password = loginRequest.getPassword();

		Future<AuthenticationResult> future = context.acquireToken("https://graph.windows.net", AZURE_TENANT_ID,
				username, password, null);
		AuthenticationResult result = future.get();

		System.out.println("Access Token - " + result.getAccessToken());
		System.out.println("Refresh Token - " + result.getRefreshToken());
		System.out.println("ID Token - " + result.getIdToken());

		return util.convertToJSON(result);
	}

	public String getAllADUsers() {
		return util.listAllMHKUsers();
	}
}
