package com.mhk.cmt.services;

import static com.mhk.cmt.util.Constants.AWS_ACCESS_KEY;
import static com.mhk.cmt.util.Constants.AWS_SECRET_KEY;
import static com.mhk.cmt.util.Constants.ROOT_AWS_ACCESS_KEY;
import static com.mhk.cmt.util.Constants.ROOT_AWS_SECRET_KEY;
import static com.mhk.cmt.util.Constants.AWS_USER_POOL_ID;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidentity.model.NotAuthorizedException;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.amazonaws.services.cognitoidp.model.AdminAddUserToGroupRequest;
import com.amazonaws.services.cognitoidp.model.AdminAddUserToGroupResult;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserResult;
import com.amazonaws.services.cognitoidp.model.AdminGetUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminGetUserResult;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthRequest;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthResult;
import com.amazonaws.services.cognitoidp.model.AttributeType;
import com.amazonaws.services.cognitoidp.model.AuthFlowType;
import com.amazonaws.services.cognitoidp.model.AuthenticationResultType;
import com.amazonaws.services.cognitoidp.model.ChangePasswordRequest;
import com.amazonaws.services.cognitoidp.model.DescribeUserPoolClientRequest;
import com.amazonaws.services.cognitoidp.model.ListUserPoolClientsRequest;
import com.amazonaws.services.cognitoidp.model.ListUserPoolClientsResult;
import com.amazonaws.services.cognitoidp.model.UserNotFoundException;
import com.amazonaws.services.cognitoidp.model.UserPoolClientType;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.identitymanagement.model.AddUserToGroupRequest;
import com.amazonaws.services.identitymanagement.model.AddUserToGroupResult;
import com.amazonaws.services.identitymanagement.model.CreateUserRequest;
import com.amazonaws.services.identitymanagement.model.CreateUserResult;
import com.amazonaws.services.identitymanagement.model.DeleteUserRequest;
import com.amazonaws.services.identitymanagement.model.DeleteUserResult;
import com.amazonaws.services.identitymanagement.model.RemoveUserFromGroupRequest;
import com.amazonaws.services.identitymanagement.model.RemoveUserFromGroupResult;
import com.amazonaws.services.identitymanagement.model.UpdateUserRequest;
import com.amazonaws.services.identitymanagement.model.UpdateUserResult;
import com.mhk.cmt.dto.AddUserRequestDTO;
import com.mhk.cmt.dto.DeleteUserDTO;
import com.mhk.cmt.dto.LoginRequestDTO;
import com.mhk.cmt.dto.PasswordRequest;
import com.mhk.cmt.dto.UpdateUserDTO;
import com.mhk.cmt.resource.UserResponse;
import com.mhk.cmt.util.AWSCognitoUtil;
//https://docs.aws.amazon.com/AWSJavaSDK/latest/javadoc/com/amazonaws/services/cognitoidp/model/AdminCreateUserRequest.html


@Service
public class UserLoginService {

	@Autowired
	private static AWSCognitoIdentityProvider cognito;
	@Autowired
	private static AmazonIdentityManagement client;
	@Autowired
	private RoleService roleService;
	@Autowired
	public AWSCognitoUtil util;
	
	
	public String loginUser(LoginRequestDTO loginRequest) {

		BasicAWSCredentials creds = new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY);

		AWSCognitoIdentityProviderClientBuilder builder = AWSCognitoIdentityProviderClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(creds));
		builder.withRegion(Regions.US_EAST_1);

		cognito = builder.build();

		AuthenticationResultType auth = null;
		String response = new String();

		ListUserPoolClientsResult listResponse = cognito.listUserPoolClients(
				new ListUserPoolClientsRequest().withUserPoolId(AWS_USER_POOL_ID).withMaxResults(1));

		if (Objects.nonNull(listResponse)) {

			UserPoolClientType userPool = cognito
					.describeUserPoolClient(new DescribeUserPoolClientRequest().withUserPoolId(AWS_USER_POOL_ID)
							.withClientId(listResponse.getUserPoolClients().get(0).getClientId()))
					.getUserPoolClient();
			try {
				Map<String, String> authParams = new HashMap<>(2);
				authParams.put("USERNAME", loginRequest.getUsername());
				authParams.put("PASSWORD", loginRequest.getPassword());
				AdminInitiateAuthRequest authRequest = new AdminInitiateAuthRequest()
						.withClientId(userPool.getClientId()).withUserPoolId(userPool.getUserPoolId())
						.withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH).withAuthParameters(authParams);
				AdminInitiateAuthResult result = cognito.adminInitiateAuth(authRequest);
				auth = result.getAuthenticationResult();
				if (Objects.nonNull(auth)) {

					response = new String("{\"idToken\":\"" + auth.getIdToken() + "\",\"access_token\":\""
							+ auth.getAccessToken() + "\"" + ",\"refresh_token\":\"" + auth.getRefreshToken() + "\"}");
				} else {

				}

			} catch (final UserNotFoundException | NotAuthorizedException exception) {
				exception.printStackTrace();
				response = "Not Authorized";
			}
		}
		return response;

	}

	public String addNewUser(AddUserRequestDTO request) {

		BasicAWSCredentials creds = new BasicAWSCredentials(ROOT_AWS_ACCESS_KEY, ROOT_AWS_SECRET_KEY);

		AWSCognitoIdentityProviderClientBuilder builder = AWSCognitoIdentityProviderClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(creds));
		builder.withRegion(Regions.US_EAST_1);

		cognito = builder.build();

		AuthenticationResultType auth = null;
		String response = new String();

		UserResponse userResponse = new UserResponse();
		// TODO: CreateUser api
		/*
		 * Create User
		 * 
		 */

		ListUserPoolClientsResult listResponse = cognito.listUserPoolClients(
				new ListUserPoolClientsRequest().withUserPoolId(AWS_USER_POOL_ID).withMaxResults(1));

		if (Objects.nonNull(listResponse)) {

			UserPoolClientType userPool = cognito
					.describeUserPoolClient(new DescribeUserPoolClientRequest().withUserPoolId(AWS_USER_POOL_ID)
							.withClientId(listResponse.getUserPoolClients().get(0).getClientId()))
					.getUserPoolClient();
			try 
			{
				String username=null;
				if(!Objects.nonNull(request.getUsername())) {
					username=request.getEmail();
				}

				AdminCreateUserRequest adminCreateUserRequest = new AdminCreateUserRequest()
						.withUserPoolId("us-east-1_9TH9mW4zJ")
						.withUsername(username)
						.withUserAttributes(new AttributeType().withName("email").withValue(request.getEmail()));
						//.withUserAttributes(new AttributeType().withName("phone_number").withValue(request.getMobileNumber()));
			
				AdminCreateUserResult curesponse=cognito.adminCreateUser(adminCreateUserRequest);
				System.err.println(util.convertToJSON(curesponse));
				if (Objects.nonNull(curesponse.getUser().getUsername())) {
					// TODO adduserToGrp :
					
					String res = addUserToGrp(request.getGroupName(), curesponse.getUser().getUsername());
					System.err.println("AddedUserToGroupResponse: " + res);
					// TODO: create user response :- user has been created successfully userinfo
					// with role name and client name
					userResponse.setUserId(curesponse.getUser().getAttributes().get(0).getValue());
					userResponse.setGroupName(request.getGroupName());
					userResponse.setEmail(request.getEmail());
					userResponse.setMobileNum(request.getMobileNumber());
					userResponse.setUsername(curesponse.getUser().getUsername());
					userResponse.setResponseCode("200");
					userResponse.setResponseMessage("Success");
					userResponse.setRoleId(roleService.getRolesByUsername(request.getUsername()));
				}

			} catch (final UserNotFoundException | NotAuthorizedException exception) {

				userResponse.setResponseCode(exception.getErrorCode());
				userResponse.setResponseMessage(exception.getErrorMessage());
			}
		}
		return util.convertToJSON(userResponse);

	}

	public String addUserToGrp(String grpName, String username) {
		
		AddUserToGroupRequest request = new AddUserToGroupRequest().withGroupName(grpName).withUserName(username);
		AdminAddUserToGroupRequest adminAddUserToGroupRequest=new AdminAddUserToGroupRequest().withGroupName(grpName).withUsername(username).withUserPoolId(AWS_USER_POOL_ID);
		AdminAddUserToGroupResult response = cognito.adminAddUserToGroup(adminAddUserToGroupRequest);
		return util.convertToJSON(response);

	}

	// TODO: Get User by Id
	public String getUserInfoByUsername(String username) {

		AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();

		AdminGetUserRequest userRequest = new AdminGetUserRequest().withUsername(username)
				.withUserPoolId(AWS_USER_POOL_ID);

		AdminGetUserResult userResult = cognitoClient.adminGetUser(userRequest);

		UserResponse userResponse = new UserResponse();
		userResponse.setUsername(userResult.getUsername());

		List<AttributeType> userAttributes = userResult.getUserAttributes();
		Map<String, String> userAttributesMap = userAttributes.stream()
				.collect(Collectors.toMap(AttributeType::getValue, AttributeType::getName));

		userResponse.setAttributes(userAttributesMap);

		return util.convertToJSON(userResponse);

	}

	public void changePassword(PasswordRequest passwordRequest) {

		AWSCognitoIdentityProvider cognitoClient = getAmazonCognitoIdentityClient();
		ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest()
				.withAccessToken(passwordRequest.getAccessToken())
				.withPreviousPassword(passwordRequest.getOldPassword())
				.withProposedPassword(passwordRequest.getPassword());

		cognitoClient.changePassword(changePasswordRequest);
		cognitoClient.shutdown();

	}

	@Bean
	private AWSCognitoIdentityProvider getAmazonCognitoIdentityClient() {

		BasicAWSCredentials creds = new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY);

		AWSCognitoIdentityProviderClientBuilder builder = AWSCognitoIdentityProviderClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(creds));
		builder.withRegion(Regions.US_EAST_1);

		return builder.build();

	}

	@Bean
	private AmazonIdentityManagement getAmazonIdentityManagement() {

		BasicAWSCredentials creds = new BasicAWSCredentials(ROOT_AWS_ACCESS_KEY, ROOT_AWS_SECRET_KEY);
		return AmazonIdentityManagementClientBuilder.standard().withRegion(Regions.US_EAST_1)
				.withCredentials(new AWSStaticCredentialsProvider(creds)).build();

	}

	public String listAllUsers(HttpHeaders request) {
		return util.listAllUsers(request);
	}

	public String deleteUser(DeleteUserDTO dto) {
		client=getAmazonIdentityManagement();
		DeleteUserRequest request = new DeleteUserRequest().withUserName(dto.getUsername());
		DeleteUserResult response = client.deleteUser(request);
		return util.convertToJSON(response);
	}

	public String updateUser(UpdateUserDTO dto) {
		client=getAmazonIdentityManagement();
		UpdateUserRequest request = new UpdateUserRequest().withUserName(dto.getOldusername())
				.withNewUserName(dto.getNewusername());
		UpdateUserResult response = client.updateUser(request);
		return util.convertToJSON(response);
	}

	public String removeUserFromGroup(String groupName, String username) {
		client=getAmazonIdentityManagement();
		RemoveUserFromGroupRequest request = new RemoveUserFromGroupRequest().withGroupName(groupName)
				.withUserName(username);
		RemoveUserFromGroupResult response = client.removeUserFromGroup(request);
		return util.convertToJSON(response);
	}
}
