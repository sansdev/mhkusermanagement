/**
 * 
 */
package com.mhk.cmt.services;

import static com.mhk.cmt.util.Constants.AWS_ACCESS_KEY;
import static com.mhk.cmt.util.Constants.AWS_SECRET_KEY;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.identitymanagement.model.DeleteRoleRequest;
import com.amazonaws.services.identitymanagement.model.DeleteRoleResult;
import com.amazonaws.services.identitymanagement.model.ListGroupsForUserRequest;
import com.amazonaws.services.identitymanagement.model.ListGroupsForUserResult;
import com.amazonaws.services.identitymanagement.model.ListRolesRequest;
import com.amazonaws.services.identitymanagement.model.ListRolesResult;
import com.amazonaws.services.identitymanagement.model.Role;
import com.mhk.cmt.util.AWSCognitoUtil;

/**
 * @author sanskriti.agarwal
 *
 */
@Service
public class RoleService {
	@Autowired
	public AWSCognitoUtil util;
	
	@Autowired
	private AmazonIdentityManagement client;
	
	@Bean
	private AmazonIdentityManagement getAmazonIdentityManagement() {

		BasicAWSCredentials creds = new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY);
		return AmazonIdentityManagementClientBuilder.standard()
				.withRegion(Regions.US_EAST_1)
				.withCredentials(new AWSStaticCredentialsProvider(creds))
				.build();

	}
	
	/*
	 * public RoleService() { client=getAmazonIdentityManagement(); }
	 */
	public String getAllRoles(ListRolesRequest r) {
	
		final ListRolesRequest listRolesRequest = new ListRolesRequest();
		  listRolesRequest.setMarker(null);
		ListRolesResult result = client.listRoles( listRolesRequest);
		List<com.mhk.cmt.model.Role> collectedRoles = new ArrayList<>();
		for(Role role: result.getRoles()) {
			if(role.getPath().equals("/")) {
				collectedRoles.add(new com.mhk.cmt.model.Role(role.getRoleId(), role.getRoleName()));
			}
		}
		return util.convertToJSON(collectedRoles);
		
	}

	public String deleteRoleById(String roleName) {
		DeleteRoleRequest request = new DeleteRoleRequest().withRoleName(roleName);
		DeleteRoleResult response = client.deleteRole(request);
		return util.convertToJSON(response);
	}

	public List<String> getRolesByUsername(String name) {
		List<String> roles = new ArrayList<>();
		
		return null;
	}
	public String listGroupForUser(String username) {
		ListGroupsForUserRequest request = new ListGroupsForUserRequest().withUserName(username);
		ListGroupsForUserResult response = client.listGroupsForUser(request);
		return util.convertToJSON(response.getGroups());
	}
}
