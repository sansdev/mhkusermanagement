/**
 * 
 */
package com.mhk.cmt.services;

/**
 * @author aaina.arora
 *
 */

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.mhk.cmt.model.Module;
import com.mhk.cmt.repository.ModuleRepository;




@Service
public class ModuleService {
	
	private static final Logger logger = Logger.getLogger(ModuleService.class.getName());
	
	@Autowired
	private ModuleRepository repository;
	
	
	public List<Module> getAllModules() {
		return repository.findAll();
	}

	
	
	public Optional<Module> findById(Long id) {
		return repository.findById(id);
	}
	
	
	public Module addNewModule(Module newModule) {
		return repository.save(newModule);
	}

	
	public void deleteModule(@PathVariable Long id) {
		repository.changeIsDeletedStatus(id);
	}
	
}
