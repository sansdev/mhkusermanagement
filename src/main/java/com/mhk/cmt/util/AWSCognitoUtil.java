/**
 * 
 */
package com.mhk.cmt.util;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.amazonaws.services.cognitoidp.model.ListUsersRequest;
import com.amazonaws.services.cognitoidp.model.ListUsersResult;
import com.amazonaws.services.cognitoidp.model.UserType;
import com.google.gson.Gson;

/**
 * @author sanskriti.agarwal
 *
 */
@Component
public class AWSCognitoUtil {

	private final static String AWS_ACCESS_KEY = "AKIAZWNCMFI7BRK6JQON";
	private final static String AWS_SECRET_KEY = "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";
	private final static String AWS_USER_POOL_ID = "us-east-1_9TH9mW4zJ";
	private final static String AWS_REGION = "us-east-1";
	
	@Autowired
	private static AWSCognitoIdentityProvider identityProvider;

	@Bean
	public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	public  String listAllUsers(HttpHeaders request) {

		String access_token = request.getFirst("access_token");
		String responseStr = "FailedMsg";
		try {
			if (!Objects.isNull(access_token)) {//validate token 
				BasicAWSCredentials creds = new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY);

				AWSCognitoIdentityProviderClientBuilder builder = AWSCognitoIdentityProviderClientBuilder.standard()
						.withCredentials(new AWSStaticCredentialsProvider(creds));
				builder.withRegion(Regions.US_EAST_1);

				identityProvider = builder.build();

				/** prepare Cognito list users request */
				ListUsersRequest listUsersRequest = new ListUsersRequest();
				listUsersRequest.withUserPoolId(AWS_USER_POOL_ID);
				
				/** send list users request */
				ListUsersResult result = identityProvider.listUsers(listUsersRequest);
				List<UserType> userTypeList = result.getUsers();
				responseStr=null;
				for (UserType ut : userTypeList) {
					responseStr += convertToJSON(ut);
				}

			}
			
		} catch (Exception e) {
			responseStr = e.getMessage();
		}
		
		return responseStr;
	}

	public String convertToJSON(Object o){
		Gson gson = new Gson();
		return gson.toJson(o);
	}

	public String listAllMHKUsers() {

		String responseStr = "FailedMsg";
		try {
			//todo
		} catch (Exception e) {
			responseStr = e.getMessage();
		}
		
		return responseStr;
	}
	
	
}
