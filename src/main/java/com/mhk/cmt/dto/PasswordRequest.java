/**
 * 
 */
package com.mhk.cmt.dto;

/**
 * @author sanskriti.agarwal
 *
 */
public class PasswordRequest {

	private String accessToken;
	private String oldPassword;
	private String password;
	
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAccessToken() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getOldPassword() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

}
