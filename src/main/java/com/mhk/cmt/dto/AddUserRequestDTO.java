/**
 * 
 */
package com.mhk.cmt.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author sanskriti.agarwal
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddUserRequestDTO {

	private String username;
	private String email;
	private String mobileNumber;
	private String groupName;
	private List<String> clientIds;
	private List<String> roleIds;

	
	

}
