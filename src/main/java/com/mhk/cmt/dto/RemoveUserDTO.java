/**
 * 
 */
package com.mhk.cmt.dto;

import lombok.Data;

/**
 * @author sanskriti.agarwal
 *
 */
@Data
public class RemoveUserDTO {

	private String groupName;
	private String username;
}
