/**
 * 
 */
package com.mhk.cmt.dto;

import lombok.Data;

/**
 * @author sanskriti.agarwal
 *
 */
@Data
public class DeleteUserDTO {

	public String username;
}
