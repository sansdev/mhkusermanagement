/**
 * 
 */
package com.mhk.cmt.dto;

import lombok.Data;

/**
 * @author sanskriti.agarwal
 *
 */
@Data
public class UpdateUserDTO {

	private String oldusername;
	private String newusername;
}
