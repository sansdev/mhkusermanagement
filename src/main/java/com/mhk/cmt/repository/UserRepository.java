/**
 * 
 */
package com.mhk.cmt.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mhk.cmt.model.User;

/**
 * @author sanskriti.agarwal
 *
 */
@Repository
@Transactional
public interface UserRepository extends CrudRepository<User, Long> {

	User findByUsername(String username);
	User findById(long id);

}
