package com.mhk.cmt.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mhk.cmt.model.UserProfile;

@Repository
public interface UserProfileRepository extends CrudRepository<UserProfile, Long> {

	Optional<UserProfile> findById(Long id);
	UserProfile findByName(String username);


}