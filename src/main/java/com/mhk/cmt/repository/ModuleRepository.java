/**
 * 
 */
package com.mhk.cmt.repository;

/**
 * @author aaina.arora
 *
 */


import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mhk.cmt.model.Module;


@Repository
public interface ModuleRepository extends JpaRepository<Module, Long> {

	  Module findById(long id); 
	  
	  @Modifying
	  @Query("update Module u set u.isDeleted=true where u.id=?1")
	  @Transactional
	  void  changeIsDeletedStatus(Long id);

}
