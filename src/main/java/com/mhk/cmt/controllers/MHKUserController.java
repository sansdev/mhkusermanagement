
package com.mhk.cmt.controllers;

import java.net.MalformedURLException;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mhk.cmt.dto.LoginRequestDTO;
import com.mhk.cmt.dto.UserAuthDTO;
import com.mhk.cmt.services.AzureADUserLoginService;
import com.mhk.cmt.util.AWSCognitoUtil;

@RestController
@RequestMapping("/admin")
@CrossOrigin(origins = "*")
public class MHKUserController {

	@Autowired
	private AzureADUserLoginService userService;
	@Autowired
	private AWSCognitoUtil util;

	@PostMapping("/login")
	public String login(@RequestBody UserAuthDTO request)
			throws MalformedURLException, InterruptedException, ExecutionException {

		LoginRequestDTO loginRequest = new LoginRequestDTO();

		loginRequest.setUsername(request.getUsername());
		loginRequest.setPassword(request.getPassword());

		return userService.loginUser(loginRequest);
	}

	// getAllMHKUsers List
	@GetMapping("/getAllADUsers")
	public String getAllADUsers() {
		return userService.getAllADUsers();
	}
}
