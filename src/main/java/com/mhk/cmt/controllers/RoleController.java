/**
 * 
 */
package com.mhk.cmt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.services.identitymanagement.model.ListRolesRequest;
import com.mhk.cmt.services.RoleService;

/**
 * @author sanskriti.agarwal
 *
 */
@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*")
public class RoleController
{
	@Autowired
	RoleService roleService;

	@GetMapping("/getAllRoles")
	private String addNewRole() {
		return roleService.getAllRoles(null);

	}

	@PostMapping("/updateRoleById/{id}")
	private void updateRoleById(@PathVariable String id) {
		// TODO Auto-generated method stub

	}

	@PostMapping("/deleteRoleById/{name}")
	private String deleteRole(@PathVariable String name) {
		return roleService.deleteRoleById(name);

	}
	
	@PostMapping("/listGroupForUser/{username}")
	private String listGroupForUser(@PathVariable String username) {
		return roleService.listGroupForUser(username);
	}
	
}
