package com.mhk.cmt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mhk.cmt.dto.AddUserRequestDTO;
import com.mhk.cmt.dto.DeleteUserDTO;
import com.mhk.cmt.dto.LoginRequestDTO;
import com.mhk.cmt.dto.RemoveUserDTO;
import com.mhk.cmt.dto.UpdateUserDTO;
import com.mhk.cmt.dto.UserAuthDTO;
import com.mhk.cmt.services.UserLoginService;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*")
public class UserController {

	@Autowired
	UserLoginService userService;

	@PostMapping("/getUserList")
	public String getAllUsers(@RequestHeader HttpHeaders request) {
		return userService.listAllUsers(request);
	}

	@PostMapping("/login")
	public String login(@RequestBody UserAuthDTO request) {

		LoginRequestDTO loginRequest = new LoginRequestDTO();
		loginRequest.setUsername(request.getUsername());
		loginRequest.setPassword(request.getPassword());
		return userService.loginUser(loginRequest);
	}

	@PostMapping("/addNewUser")
	public String addNewUser(@RequestBody AddUserRequestDTO dto) {
		return userService.addNewUser(dto);
	}
	
	@PostMapping("/deleteUser")
	public String deleteUser(@RequestBody DeleteUserDTO dto) {
		return userService.deleteUser(dto);
	}

	@PostMapping("/updateUser")
	public String updateUser(@RequestBody UpdateUserDTO dto) {
		return userService.updateUser(dto);
	}

	@PostMapping("/removeUserFromGroup")
	public String removeUserFromGroup(@RequestBody RemoveUserDTO dto) {
		return userService.removeUserFromGroup(dto.getGroupName(),dto.getUsername());
	}
	

}
