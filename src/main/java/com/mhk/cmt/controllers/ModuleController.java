/**
 * 
 */
package com.mhk.cmt.controllers;

/**
 * @author aaina.arora
 *
 */

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.mhk.cmt.model.Module;
import com.mhk.cmt.services.ModuleService;


@RestController
@CrossOrigin(origins = "*")
public class ModuleController {
	private static final Logger logger = Logger.getLogger(ModuleController.class.getName());

	@Autowired
	private ModuleService moduleService;

	@GetMapping(value = "/getModuleList")
	public List<Module> getAllModules() {
		logger.info("get module list");
		return moduleService.getAllModules();
	}

	@PostMapping("/addNewModule")
	public Module addNewModule(@RequestBody Module newModule) {
		logger.info("add new module");
		newModule.setModuleName(newModule.getModuleName().toUpperCase());
		return moduleService.addNewModule(newModule);
	}

	@PutMapping("/updateModuleById/{id}")
	public Module updateModule(@RequestBody Module newModule, @PathVariable Long id) {
		logger.info("update module");
		return moduleService.findById(id).map(module -> {
			module.setModuleName(newModule.getModuleName().toUpperCase());
			module.setDescription(newModule.getDescription());
			module.setStatus(newModule.getStatus());
			return moduleService.addNewModule(module);
		}).orElseGet(() -> {
			newModule.setId(id);
			return moduleService.addNewModule(newModule);
		});
	}

	@DeleteMapping("/deleteModuleById/{id}")
	public ResponseEntity<Object> deleteModule(@PathVariable Long id) {
		logger.info("deleting module");
		moduleService.deleteModule(id);
		return new ResponseEntity<>("Module is deleted.", null, HttpStatus.OK);
	}

}