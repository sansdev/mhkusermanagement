package com.mhk.cmt.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {
	
	private String responseCode;
	private String responseMessage;
	private String username;
	private String userId;
	private String email;
	private String mobileNum;
	private String groupName;
	private List<String> clientId;
	private List<String> roleId;
	Map<String, String> attributes = new HashMap<>();
	
	
}
